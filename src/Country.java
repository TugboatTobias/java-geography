public class Country{
    private String name;
    private String capital;
    private int area;
    private int population;
    private String currency;

    public Country(String name, String capital, int area, int population, String currency) {
        this.name = name;
        this.capital = capital;
        this.area = area;
        this.population = population;
        this.currency = currency;
    }

    public String name() { return name; }
    public String capital() { return capital; }
    public int area() { return area; }
    public int population() { return population; }
    public String currency() { return currency; }
    public int density(){ return population / area; }
}
